﻿using BookingRoomModels;
using BookingRoomServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BookingRoomApi.Controllers
{
    public class BookingController : ApiController
    {
        private readonly IBookingServices _bookingServices;
        private readonly IRoomServices _roomServices;

        public BookingController(IBookingServices bookingServices, IRoomServices roomServices)
        {
            _bookingServices = bookingServices;
            _roomServices = roomServices;
        }

        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        //Test with PostMan
        [HttpPost]
        public BookingResponse Post([FromBody]Booking booking)
        {
            if (_roomServices.IsNoConflict(booking))
            {
                _bookingServices.Add(booking);
            }
            else
            {
                var room = _roomServices.Get(booking.RoomNumber);
                var availableHours = _roomServices.GetAvailableHours(room);
                return new BookingResponse { Statut = -1, Message = "Conflict with another booking",AvailableHours = availableHours };
            }
            return new BookingResponse { Statut = 0, Message = "Sucess" };
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
            _bookingServices.Delete(id);
        }
    }
}
