﻿using BookingRoomModels;
using BookingRoomServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BookingRoomApi.Controllers
{
    public class RoomController : ApiController
    {
        private readonly IRoomServices _roomServices;
        public RoomController(IRoomServices roomServices)
        {
            _roomServices = roomServices;
        }
        // GET api/values
        public IEnumerable<Room> Get()
        {
            return _roomServices.GetAll();
        }
    }
}
