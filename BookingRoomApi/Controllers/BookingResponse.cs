﻿using System.Collections.Generic;

namespace BookingRoomApi.Controllers
{
    public class BookingResponse
    {
        public int Statut;
        public string Message;
        public IEnumerable<int> AvailableHours;
    }
}