﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BookingRoomApi;
using BookingRoomApi.Controllers;
using BookingRoomServices;
using BookingRoomDal;
using BookingRoomModels;

namespace BookingRoomApi.Tests.Controllers
{
    [TestClass]
    public class RoomControllerTest
    {
        [TestMethod]
        public void GetAll()
        {
            // Réorganiser
            RoomController controller = new RoomController(new RoomServices(RoomRepository.Instance));

            // Agir
            IEnumerable<Room> result = controller.Get();

            // Déclarer
            Assert.IsNotNull(result);
            Assert.AreEqual(10, result.Count());
        }  
    }
}
